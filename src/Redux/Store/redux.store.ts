import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import Reducers from '../Reducer';

export default () => ({
    ...createStore(Reducers,
        composeWithDevTools())
});