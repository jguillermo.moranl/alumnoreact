import React from "react";
import Auxiliar from "../../Hoc/auxiliar";
import { useSelector } from "react-redux";

const Lista = (props: any) => {
  const stateLista = useSelector((state: any) => state.Alumno.lista);

  const listaAlumnos = stateLista.map((datos: any) => (
    <a href="#" className="list-group-item list-group-item-action">
      {datos.nombre} | {datos.edad} | {datos.curso}
    </a>
  ));

  return (
    <Auxiliar>
      <div className="list-group">{listaAlumnos}</div>
    </Auxiliar>
  );
};
export default Lista;
