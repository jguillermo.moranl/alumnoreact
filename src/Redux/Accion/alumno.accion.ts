import Ipayload from '../../interfaces/payload.interface'
import Ialumno from '../../interfaces/alumno.interface'
import {
    ALUMNO_NUEVO,
    ALUMNO_MODIFICAR,
    ALUMNO_ELIMINAR
} from '../Types/alumno.types';

const AlumnoNuevo = (data: any): Ipayload => ({
    type: ALUMNO_NUEVO,
    data
});

const AlumnoModificar = (data: Ialumno, index: number): Ipayload => ({
    type: ALUMNO_MODIFICAR,
    data: {
        alumno: data,
        index
    }
});

const AlumnoEliminar = (index: number): Ipayload => ({
    type: ALUMNO_ELIMINAR,
    data: index
});


export {
    AlumnoNuevo,
    AlumnoModificar,
    AlumnoEliminar
}