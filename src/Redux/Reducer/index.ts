import { combineReducers } from 'redux';
import Alumno from './alumno.reducer';
import ReduxDevTools from 'redux-devtools-extension';

export default combineReducers({
    Alumno
})