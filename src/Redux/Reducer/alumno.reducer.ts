import {
    ALUMNO_NUEVO,
    ALUMNO_MODIFICAR,
    ALUMNO_ELIMINAR
} from '../Types/alumno.types';
import Ialumno from '../../interfaces/alumno.interface';
import Ipayload from '../../interfaces/payload.interface';

const StateAlumno = {
    lista: [{ curso: '3/A', edad: 16, nombre: 'Manfinfla' }] as Ialumno[],
}

export default (
    state = StateAlumno,
    acciones: { type: string; data: any }, 
    ) => {
    switch (acciones.type) {
        case ALUMNO_NUEVO:
            return {
                lista: [...state.lista, acciones.data]
            };
        case ALUMNO_MODIFICAR:
            const alumno: Ialumno = state.lista[acciones.data.index];
            const malumno: Ialumno = { ...alumno, ...acciones.data.alumno }
            const lista = state.lista;
            lista[acciones.data.index] = malumno;
            return {
                lista: [...lista]
            }
        case ALUMNO_ELIMINAR:
            const nuevaLista = state.lista.splice(acciones.data, 1);
            return {
                lista: nuevaLista
            }
        default:
            return state;
    }
}