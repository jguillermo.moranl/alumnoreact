import "./App.css";
import React from "react";
import { Provider } from "react-redux";
import StoreAlumnos from "./Redux/Store/redux.store";
import Lista from "./Alumnos/Lista/Lista";
import InfoAlumno from "./Alumnos/informacion/InfoAlumno";
import FormAlumno from "./Alumnos/Formulario/FormAlumno";

function App() {
  return (
    <Provider store={StoreAlumnos()}>
      <div className="contenedor-alumno">
        <Lista />
        <InfoAlumno />
        <FormAlumno />
      </div>
    </Provider>
  );
}

export default App;
